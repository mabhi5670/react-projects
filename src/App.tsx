import { useState } from 'react';
import styles from './App.module.scss';
import Sidebar from "./components/Sidebar/Sidebar";
import Profile from './Pages/MY_profile/My_profile';
import Academics from './Pages/My_Academics/My_Academics';
import Hobbies from './Pages/My_Hobbies/My_Hobbies';
import Certificate from './Pages/My_certificate/My_certificate';
import {Route} from 'react-router-dom';

// if page === '' LOAD homepage
// if page === 'about' LOAD ABOUT US page

const App = () => {

  const [page, setPage] = useState<string>('');

  const navigateTo = (url: string) => setPage(url);


  return (
    <>
      <Sidebar
        navigateTo={navigateTo}
      />
      <main>
        <Route path="/" exact>
        <Profile />

        </Route>

        <Route path="/academics">
        <Academics />
        </Route>

        <Route path="/hobbies">
        <Hobbies />
        </Route>

        <Route path="/certificates">
        <Certificate />
        </Route>

        {//{
        //   page === '' && <Profile />
        // }

        // {
        //   page === 'academics' && <Academics />
        // }

        // {
        //   page === 'hobbies' && <Hobbies />
        // }

        // {
        //   page === 'certificates' && <Certificate />
        // }
        }
      </main>
    </>
  )
}

export default App;

import styles from './Pill.module.scss';
import { IProps } from './Pill.types';

const Pill = ({ text}: IProps) => <span 
    className={styles.bars1}
>{text}</span>

export default Pill;

import Pill from '../Pill/Pill';
import styles from './Sidebar.module.scss';
import { IProps } from './Sidebar.types';
import {Link} from 'react-router-dom';

const Header = ({ navigateTo }: IProps) => {
    return (
        <header className={styles.bars}>
            <nav>
                <Link to="/">
                <Pill text="My_profile" />
                </Link>
                
                <Link to="/academics">
                <Pill text="Academics" />
                </Link>
                
                <Link to="/certificates">
                <Pill text="Certificates" />
                </Link>
                
                <Link to="/hobbies">
                <Pill text="Hobbies" />
                </Link>
            </nav>
        </header>
    )
};

export default Header;

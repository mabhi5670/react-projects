export interface IProps {
    navigateTo: (url: string) => void;
}
